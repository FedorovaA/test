﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WS1.Models;

namespace WS1.DAO
{
    class LoginController
    {
        public User user { get; set; }
        private static LoginController instance;
        public static LoginController GetInstance()
        {
            if(instance==null)
            {
                instance = new LoginController();
            }
            return instance;
        }
        private LoginController()
        {
           
        }

    }
}
