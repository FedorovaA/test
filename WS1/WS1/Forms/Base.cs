﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WS1.Forms;

namespace WS1.Forms
{
    public partial class Base : Form
    {
        //protected string connectionString = @"Data Source=DESKTOP-ML9UEEC;Initial Catalog=test;Integrated Security=True";
        //protected SqlConnection sqlConnection;
        //protected SqlCommand sqlCommand;
        public Base()
        {
            InitializeComponent();
        }
        protected void showNextForm(Base nextForm,bool isClose=true)
        {
            this.Hide();
            nextForm.StartPosition = FormStartPosition.Manual;
            nextForm.Location = this.Location;
            nextForm.ShowDialog();
            if (isClose)
            {
                this.Close();
            }
            else
            {
                this.StartPosition = FormStartPosition.Manual;
                this.Location = nextForm.Location;
                this.Show();
            }
        }
    }
}
