﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WS1.DAO;
using WS1.Models;

namespace WS1.Forms
{
    public partial class ListForm : Base
    {
        public ListForm()
        {
            InitializeComponent();
            var users = UserDAO.GetUsers();
            users.Sort(delegate (User us1, User us2) { return us2.lastName.CompareTo(us1.lastName); });
            comboBox1.DataSource = users;
            comboBox1.DisplayMember = "LastName";
            comboBox1.ValueMember = "Login";
            comboBox1.SelectedIndexChanged += comboBox1_SelectedIndexChanged;

            listBox1.DisplayMember = "Login";
            listBox1.ValueMember = "Role";
            var users2 = UserDAO.GetUsers();
            listBox1.DataSource = users2;
            listBox1.SelectedIndexChanged += listBox1_SelectedIndexChanged;
            listView1.Items.AddRange(users.Select(u =>
            {
                var item = new ListViewItem();
                item.Text = u.lastName;
                item.Tag = u;
                item.ImageKey = u.login;
                return item;
            }).ToArray());

            var avatars = new ImageList();
            foreach(User u in users)
            {
                avatars.Images.Add(u.login, UserDAO.GetImageFromFile(u.photo));
            }
            avatars.ImageSize = new Size(128, 128);
            avatars.ColorDepth = ColorDepth.Depth32Bit;
            listView1.LargeImageList = avatars;
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string s = (comboBox1.SelectedValue as string);
            comboBoxLabel.Text = (comboBox1.SelectedValue as string);
            var users = UserDAO.GetUsers();
            for (int i = 0; i < users.Count; i++)
            {
                if (users[i].login == s)
                {
                    pictureBox1.Image = Image.FromFile(users[i].photo);
                }
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBoxLabel.Text = (listBox1.SelectedItem as User).firstNameAndPatr;
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            listViewLabel.Text = "";
            foreach(ListViewItem lvi in listView1.SelectedItems)
            {
                listViewLabel.Text += (lvi.Tag as User).login + " ";
            }
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            // string log = comboBox1.SelectedValue as string;
            //pictureBox1.Image = Image.FromFile("Images\\" + log + ".jpg");
            //var users = UserDAO.GetUsers();
            //for (int i = 0; i < users.Count; i++)
            //{
            //    if (users[i].login.Equals(comboBox1.SelectedIndex))
            //    {
            //        comboBoxLabel.Text = "yes";
            //    }
            //}
        }
    }
}
