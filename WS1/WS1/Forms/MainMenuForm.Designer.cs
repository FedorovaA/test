﻿namespace WS1.Forms
{
    partial class MainMenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.userName = new System.Windows.Forms.Label();
            this.listButton = new System.Windows.Forms.Button();
            this.tablesButton = new System.Windows.Forms.Button();
            this.imageButton = new System.Windows.Forms.Button();
            this.othersButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // userName
            // 
            this.userName.AutoSize = true;
            this.userName.Location = new System.Drawing.Point(163, 111);
            this.userName.Name = "userName";
            this.userName.Size = new System.Drawing.Size(51, 20);
            this.userName.TabIndex = 0;
            this.userName.Text = "label1";
            // 
            // listButton
            // 
            this.listButton.Location = new System.Drawing.Point(167, 160);
            this.listButton.Name = "listButton";
            this.listButton.Size = new System.Drawing.Size(178, 44);
            this.listButton.TabIndex = 1;
            this.listButton.Text = "Списки";
            this.listButton.UseVisualStyleBackColor = true;
            this.listButton.Click += new System.EventHandler(this.listButton_Click);
            // 
            // tablesButton
            // 
            this.tablesButton.Location = new System.Drawing.Point(167, 221);
            this.tablesButton.Name = "tablesButton";
            this.tablesButton.Size = new System.Drawing.Size(178, 52);
            this.tablesButton.TabIndex = 2;
            this.tablesButton.Text = "Таблицы";
            this.tablesButton.UseVisualStyleBackColor = true;
            this.tablesButton.Click += new System.EventHandler(this.tablesButton_Click);
            // 
            // imageButton
            // 
            this.imageButton.Location = new System.Drawing.Point(167, 297);
            this.imageButton.Name = "imageButton";
            this.imageButton.Size = new System.Drawing.Size(178, 64);
            this.imageButton.TabIndex = 3;
            this.imageButton.Text = "Изображения";
            this.imageButton.UseVisualStyleBackColor = true;
            // 
            // othersButton
            // 
            this.othersButton.Location = new System.Drawing.Point(167, 384);
            this.othersButton.Name = "othersButton";
            this.othersButton.Size = new System.Drawing.Size(178, 55);
            this.othersButton.TabIndex = 4;
            this.othersButton.Text = "Другое";
            this.othersButton.UseVisualStyleBackColor = true;
            // 
            // MainMenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 572);
            this.Controls.Add(this.othersButton);
            this.Controls.Add(this.imageButton);
            this.Controls.Add(this.tablesButton);
            this.Controls.Add(this.listButton);
            this.Controls.Add(this.userName);
            this.Name = "MainMenuForm";
            this.Text = "MainMenuForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label userName;
        private System.Windows.Forms.Button listButton;
        private System.Windows.Forms.Button tablesButton;
        private System.Windows.Forms.Button imageButton;
        private System.Windows.Forms.Button othersButton;
    }
}