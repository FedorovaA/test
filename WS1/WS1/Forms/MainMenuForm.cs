﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WS1.DAO;

namespace WS1.Forms
{
    public partial class MainMenuForm : Base
    {
        public MainMenuForm()
        {
            InitializeComponent();
            userName.Text = LoginController.GetInstance().user.login;
        }

        private void listButton_Click(object sender, EventArgs e)
        {
            var listForm = new ListForm();
            showNextForm(listForm, false);
        }

        private void tablesButton_Click(object sender, EventArgs e)
        {
            var tableForm = new TableForm();
            showNextForm(tableForm, false);
        }
    }
}
