﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WS1.Forms;
using System.Data.SqlClient;
using WS1.DAO;

namespace WS1
{
    public partial class signInForm : Base
    {
        public signInForm()
        {
            InitializeComponent();
        }

        private void signInButton_Click(object sender, EventArgs e)
        {
            string loginUser = loginTextBox.Text;
            string passwordUser = passwordTextBox.Text;
            var user = UserDAO.GetUsers().FirstOrDefault(u => u.login.Equals(loginUser) && u.password.Equals(passwordUser));
            if(user!=null)
            {
                LoginController.GetInstance().user = user;
                var menu = new MainMenuForm();
                showNextForm(menu);
            }
            else
            {
                messageLabel.Text = "Access denied";
            }

            //sqlConnection = new SqlConnection(connectionString);
            //sqlConnection.Open();
            //sqlCommand = new SqlCommand();
            //sqlCommand.Connection = sqlConnection;
            //sqlCommand.CommandText = "SELECT [login],[password] FROM [dbo].[Student] WHERE [login]=@log AND [password]=@pas";
            //sqlCommand.Parameters.Add("log", SqlDbType.NVarChar).Value = loginUser;
            //sqlCommand.Parameters.Add("pas", SqlDbType.NVarChar).Value = passwordUser;
            //SqlDataReader rd = sqlCommand.ExecuteReader();
            //if (rd.HasRows)
            //{
            //    loginTextBox.Text = "hello";
            //}
            //else
            //{
            //    loginTextBox.Text = "no";
            //}
            //sqlConnection.Close();
        }
    }
}
