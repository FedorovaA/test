﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WS1.DAO;
using WS1.Models;

namespace WS1.Forms
{
    public partial class TableForm : Base
    {
        public TableForm()
        {
            InitializeComponent();
            DataTable dt = new DataTable();
            dt.Columns.Add("Last Name");
            dt.Columns.Add("First Name");
            dt.Columns.Add("Role");
            dt.Columns.Add("Avatar", typeof(Bitmap));
            var users = UserDAO.GetUsers();
            foreach (User u in users)
            {
                DataRow dr = dt.NewRow();
                dr["Last Name"] = u.lastName;
                dr["First Name"] = u.firstNameAndPatr;
                dr["Role"] = u.role;
                dr["Avatar"] = UserDAO.GetImageFromFile(u.photo);
                dt.Rows.Add(dr);
            }
            dataGridView1.DataSource = dt;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string s = dataGridView1[0,dataGridView1.CurrentCell.RowIndex].Value.ToString();
            string s1 = dataGridView1[1, dataGridView1.CurrentCell.RowIndex].Value.ToString();
            var users = UserDAO.GetUsers();
            for (int i = 0; i < users.Count; i++)
            {
                if (users[i].firstNameAndPatr == s1 && users[i].lastName==s)
                {
                    pictureBox1.Image = Image.FromFile(users[i].photo);
                }
            }
        }

        private void dataGridView1_Click(object sender, EventArgs e)
        {
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Last Name");
            dt.Columns.Add("First Name");
            dt.Columns.Add("Role");
            dt.Columns.Add("Avatar", typeof(Bitmap));
            if (comboBox1.Text == "фамилия")
            {
                dataGridView1.Sort(dataGridView1.Columns[0], ListSortDirection.Ascending);
            }
            if(comboBox1.Text=="имя и отчество")
            {
                dataGridView1.Sort(dataGridView1.Columns[1], ListSortDirection.Descending);
            }
            if (comboBox2.Text=="по фамилии")
            {
                var users2 = UserDAO.GetUsers();
                for (int i=0;i<users2.Count;i++)
                {
                    if (users2[i].lastName== textBox1.Text)
                    {
                        DataRow dr = dt.NewRow();
                        dr["Last Name"] = users2[i].lastName;
                        dr["First Name"] = users2[i].firstNameAndPatr;
                        dr["Role"] = users2[i].role;
                        dr["Avatar"] = UserDAO.GetImageFromFile(users2[i].photo);
                        dt.Rows.Add(dr);
                        //label1.Text = label1.Text + users2[i].lastName;

                    }
            }
                dataGridView1.DataSource = dt;
            }
            try
            {
                StreamWriter StreamW = new StreamWriter("Resources\\users1.csv",true,Encoding.UTF8);
                var users = UserDAO.GetUsers();
                for (int i=0;i<users.Count;i++)
                {
                    string save = users[i].lastName + ',' + users[i].firstNameAndPatr + ',' + users[i].login + ',' + users[i].password + ',' + users[i].role + ',' + users[i].photo;
                    StreamW.WriteLine(save);
                }
                StreamW.Close();
            }
            catch (Exception ex)
            {
            }

        }
    }
}
