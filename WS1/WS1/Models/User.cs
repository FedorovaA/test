﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WS1.Models;

namespace WS1.Models
{
    class User
    {
        public string lastName { get; set; }
        public string firstNameAndPatr { get; set; }
        public string login { get; set; }
        public string password { get; set; }
        public string role { get; set; }
        public string photo { get; set; }
    }
}
